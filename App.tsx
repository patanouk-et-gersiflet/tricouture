import { useFonts } from "expo-font";
import { useEffect } from "react";
import { View } from "react-native";
import { Button, TamaguiProvider } from "tamagui";

import config from "./tamagui.config";

export default function App() {
  const [loaded] = useFonts({
    Inter: require("@tamagui/font-inter/otf/Inter-Medium.otf"),
    InterBold: require("@tamagui/font-inter/otf/Inter-Bold.otf"),
  });

  useEffect(() => {
    if (loaded) {
      // can hide splash screen here
    }
  }, [loaded]);

  if (!loaded) {
    return null;
  }

  return (
    <TamaguiProvider config={config}>
      <View className="flex-1 items-center justify-center bg-white">
        <Button>Hello world</Button>
      </View>
    </TamaguiProvider>
  );
}
